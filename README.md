# Acacallis

`/æ̀kəkǽləs/`

[![build status](
https://gitlab.com/grauwoelfchen/acacallis/badges/master/build.svg)](
https://gitlab.com/grauwoelfchen/acacallis/commits/master)

```txt
Acacallis; A Cute Acme ChALLenge applIcation Server
```

This project is a tiny **acme** challenge server application as a _tool_
to issue ssl certificate(s) of Let's Encrypt (using `certbot`).


## Setup

```zsh
# setup Python environment (e.g. virtualenv)
% python3.4 -m venv venv
% source venv/bin/activate

(venv) % pip install --upgrade pip setuptools
```

### Install packages

```zsh
# via pypi
(venv) % pip install -r requirements.txt -c constraints.txt
```

### Run server

```zsh
(venv) % python app.py
```


## Production

Set environment vars, correctly. (See `.env.sample`)

* `CHALLENGER_TYPE` python or node.js
* `CHALLENGER_RESPONSE` comma separated response(s)

```zsh
# or use Procfile
(venv) % CHALLENGER_TYPE="python" ./run
```


## Using Certbot

Run certbot with `--manual`.  
Or check its document.

```zsh
% certbot certonly -d <domain> -d <domain> --manual \
--config-dir <dir> --logs-dir <dir> --work-dir <dir>
```


## Integration

See [NOTE.md](NOTE.md).


## Development

```zsh
(venv) % python test.py
```

## Link

* [Let's Encrypt](https://letsencrypt.org/)
* [certbot](https://certbot.eff.org/)



## License

Copyright (c) 2016-2017 Yasuhiro Asaka

This is free software;  
You can redistribute it and/or modify it under the terms of
the GNU Affero General Public License (AGPL).

See [LICENSE](LICENSE).
