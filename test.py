import unittest
from unittest import mock

from pyramid import testing


class ViewUnitTests(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def test_challenge_missing_challenger_response(self):
        import os
        from app import challenge

        # CHALLENGER_RESPONSE
        m = mock.Mock()
        m.return_value = ''
        os.environ.get = m

        req = testing.DummyRequest()

        req.matchdict = {'content': ''}
        res = challenge(req)
        self.assertEqual(':\'(', res)

    def test_challenge_invalid_challenger_response(self):
        import os
        from app import challenge

        # CHALLENGER_RESPONSE
        m = mock.Mock()
        m.return_value = 'foobarbaz'
        os.environ.get = m

        req = testing.DummyRequest()

        with self.assertRaises(ValueError):
            req.matchdict = {'content': 'foo'}
            challenge(req)

    def test_challenge_unknown_content(self):
        import os
        from app import challenge

        # CHALLENGER_RESPONSE
        m = mock.Mock()
        m.return_value = 'bar.baz'
        os.environ.get = m

        req = testing.DummyRequest()

        req.matchdict = {'content': 'foo'}
        res = challenge(req)
        self.assertEqual(':\'(', res)

    def test_challenge_single_content_lookup(self):
        import os
        from app import challenge

        # CHALLENGER_RESPONSE
        m = mock.Mock()
        m.return_value = 'foo.bar'
        os.environ.get = m

        req = testing.DummyRequest()

        req.matchdict = {'content': 'foo'}
        res = challenge(req)
        self.assertEqual('foo.bar', res)

        m = mock.Mock()
        m.return_value = 'foo.bar,'
        os.environ.get = m

        req = testing.DummyRequest()

        req.matchdict = {'content': 'foo'}
        res = challenge(req)
        self.assertEqual('foo.bar', res)

    def test_challenge_multiple_contents_lookup(self):
        import os
        from app import challenge

        # CHALLENGER_RESPONSE
        m = mock.Mock()
        m.return_value = 'foo.bar,qux.quux'
        os.environ.get = m

        req = testing.DummyRequest()

        req.matchdict = {'content': 'foo'}
        res = challenge(req)
        self.assertEqual('foo.bar', res)

        req.matchdict = {'content': 'qux'}
        res = challenge(req)
        self.assertEqual('qux.quux', res)


class ViewFunctionalTests(unittest.TestCase):
    def setUp(self):
        from app import main
        from webtest import TestApp

        _app = main()
        self.testapp = TestApp(_app)

    def test_root(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue(b':-D' in res.body)

    def test_not_found(self):
        res = self.testapp.get('/not-found', status=404)
        self.assertTrue(b'Cannot GET /not-found' in res.body)

    def test_not_found_with_without_content(self):
        content = ''
        path = '/.well-known/acme-challenge/{}'.format(content)
        res = self.testapp.get(path, status=404)
        self.assertTrue(b'Cannot GET /.well-known/acme-challenge' in res.body)

    def test_error_with_value_error_by_invalid_challenger_response(self):
        import os

        # CHALLENGER_RESPONSE
        m = mock.Mock()
        m.return_value = 'foobarbaz'
        os.environ.get = m

        with self.assertRaises(ValueError):
            content = 'foo'
            path = '/.well-known/acme-challenge/{}'.format(content)
            self.testapp.get(path)


    def test_challenge(self):
        import os

        # CHALLENGER_RESPONSE
        m = mock.Mock()
        m.return_value = 'foo.bar'
        os.environ.get = m

        content = 'foo'
        path = '/.well-known/acme-challenge/{}'.format(content)
        res = self.testapp.get(path, status=200)
        self.assertTrue(b'foo.bar' in res.body)

if __name__ == '__main__':
    unittest.main()
