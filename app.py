import os
import logging

from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.httpexceptions import HTTPNotFound
from pyramid.response import Response
from pyramid.view import view_config


dotenv_file = os.path.join(os.getcwd(), '.env')
if os.path.isfile(dotenv_file):
    from dotenv import load_dotenv
    load_dotenv(dotenv_file)


@view_config(context=HTTPNotFound, renderer='string')
def not_found(req):
    body = 'Cannot {} {}'.format(req.method, req.path)
    return Response(body, status='404 Not Found')

@view_config(route_name='challenge', renderer='string')
def challenge(req):
    content = req.matchdict['content']
    cr = os.environ.get('CHALLENGER_RESPONSE', '.')
    res = ':\'('
    if cr:
        data = dict(v.split('.') for v in
                    [r for r in cr.split(',') if r != ''])
        if content in data:
          res = '{0!s}.{1!s}'.format(content, data[content])
    return res

@view_config(route_name='root', renderer='string')
def root(req):
    return ':-D'

def main():
    config = Configurator()
    config.add_route('root', '/')
    config.add_route('challenge', '/.well-known/acme-challenge/{content}')
    config.scan()
    app = config.make_wsgi_app()
    return app


if __name__ == '__main__':
    app = main()

    port = int(os.environ.get('PORT', '5000'))
    server = make_server('0.0.0.0', port, app)

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    sh = logging.StreamHandler()
    sh.setLevel(logging.INFO)
    sh.setFormatter(logging.Formatter('%(message)s'))

    logger.addHandler(sh)
    logger.info('listing on port {0:d}'.format(port))

    server.serve_forever()
