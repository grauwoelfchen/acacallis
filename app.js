var fs = require('fs')
  , path = require('path')
  ;

var express = require('express')
  ;

var dotenv_file = path.resolve(process.cwd(), '.env');
if (fs.existsSync(dotenv_file)) {
  var dotenv = require('dotenv');
  dotenv.config({path: dotenv_file});
}


var app = express()
  , port = process.env.PORT || 5000
  ;

app.get('/.well-known/acme-challenge/:content', function(req, res) {
  return res.send(process.env.CHALLENGER_RESPONSE || '');
});

app.listen(port, function() {
  console.log('listing on port ' + port);
});
