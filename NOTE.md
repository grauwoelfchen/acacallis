# Integration


## Your server

```zsh
: let\'s do it as you like
% :-)
```


## Heroku

See `Procfile`.  
The following snippet is a example with python.

```zsh
% git checkout -b <RELEASE-BRANCH>

: set buildpack
% heroku buildpacks:set heroku/python --remote <REMOTE>

: deploy
% git push <REMOTE> <RELEASE-BRANCH>:master --force

: run
% heroku config:set CHALLENGER_TYPE="python" --app=<APP>
```

```zsh
% cd /path/to/certs/
% mkdir <DIR>
% certbot certonly -d <DOMAIN> -d <DOMAIN> --manual \
--config-dir <DIR> \
--logs-dir <DIR> \
--work-dir <DIR>

: set value as CHALLENGER_RESPONSE
% heroku config:set CHALLENGER_RESPONSE="..." --app=<APP>
: tip; concat responses like below for multiple domains while interaction
% heroku config:set CHALLENGER_RESPONSE="xxx,xxx"
```

And then, answer `Y` to certbot!


## Cleanup

Unset `CHALLENGER_XXX` vars.

```zsh
% heroku config:unset CHALLENGER_TYPE --app=<APP>
% heroku config:unset CHALLENGER_RESPONSE --app=<APP>
```


## (Re)Set new certificate

```zsh
: specify path to cert.pem and privkey.pem
% heroku certs:update \
  <DIR>/live/<DOMAIN>/cert.pem \
  <DIR>/live/<DOMAIN>/privkey.pem \
  --app <APP>
```


## Restore application

Finally, reset your buildpack with `heroku buildpacks:set` and deploy
application again.

```zsh
% cd /path/to/original/application

: e.g. heroku-buildpack-multi
% heroku buildpacks:set \
  https://github.com/heroku/heroku-buildpack-multi.git

: push original application with `--force`
% git push heroku release:master --force
```

Your original application may need `purge_cache`.

```zsh
: Use heroku\'s `repo` plugin
% heroku repo:purge_cache --app=<APP>
```
